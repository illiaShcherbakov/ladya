<?php get_header(); ?>


<div class="main col-lg-10 col-xs-12">
<!--BLOG SECTION START-->
    <section class="blog-section col-lg-12">
        <h1 class="blog-section__title"><?php _e('Latest in Blog', 'ladya')?></h1>


<?php
 $eventcat = get_category_by_slug('events');
 $eventcat_id = $eventcat->term_id;
 $ptcat = get_category_by_slug('pt');
 $ptcat_id = $ptcat->term_id;
function pt_event_check($cat1, $cat2) {
    $check_query = new WP_Query( "'cat =" . $ca1 . "," . $cat2 . "'");
    $post_per_page;
    if( $check_query->have_posts() ) {
    return 8;
 } else {
        return 6;
    }
}

 $args = array(
    'cat' => "'-". $eventcat_id.", -". $ptcat_id."'",
    'posts_per_page' => pt_event_check($eventcat_id, $ptcat_id),
    'order' => 'DESC'
 );
    $query = new WP_Query( $args );

    $i=$z=0;
?>

                <div class="blog-container col-lg-12">

                <div class="blog-container__main col-lg-12 row">
        <?php while ( $query->have_posts() ) : $query->the_post(); ?>

        <article <?php post_class('col-lg-6'); ?>>

        <?php if (has_post_thumbnail()) { ?>
            <a href="<?php get_permalink(); ?>" class="post-thumbnail col-lg-12"><?php the_post_thumbnail(); ?></a>


                      <?php } else { ?>
                       <a href="<?php get_permalink(); ?>" class="post-thumbnail default col-lg-12"><img src="<?php bloginfo('template_url'); ?>/images/default-thumbnail.jpg" alt="<?php the_title(); ?>"></a>
                       <?php }; ?>

        <div class="description">
            <div class="description__container">
           <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

           <div class="post-info">
                       <span class="post-info__category"><?php _e('Posted in', 'ladya'); echo ' '; the_category(', '); ?> </span>
                        <span class="post-info__date"><?php the_time('j.m.Y'); ?></span>
                </div>

           <span class="excerpt"><?php
               //$content = apply_filters( 'the_content', get_the_content() );
                //$content = str_replace( ']]>', ']]&gt;', $content );
//echo $content;
               the_excerpt();
               ?> </span>
            </div>
        </div>
        </article><!-- End of pt div-->
        <?php
        if ($i++ == 1) {
            break;
        }

         endwhile;  ?>
        </div>

           <div class="blog-container__rest col-lg-12 row">
               <?php if (!empty($query) && $query->have_posts()): ?>

               <?php while ($query->have_posts()): $query->the_post(); $z++;?>

               <article <?php post_class('col-lg-3'); ?>>

            <?php if (has_post_thumbnail()) { ?>
            <a href="<?php get_permalink(); ?>" class="post-thumbnail"><?php the_post_thumbnail(); ?></a>


                      <?php } else { ?>
                       <a href="<?php get_permalink(); ?>" class="post-thumbnail default"><img src="<?php bloginfo('template_url'); ?>/images/default-thumbnail.jpg" alt="<?php the_title(); ?>"></a>
                       <?php }; ?>

        <div class="title">
           <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
           <div class="post-info">
                       <span class="post-info__category"><?php _e('Posted in', 'ladya'); echo ' '; the_category(', '); ?> </span>
                        <span class="post-info__date"><?php the_time('j.m.Y'); ?></span>
                </div>
        </div>

        </article><!-- End of pt div-->



           <?php endwhile; endif; wp_reset_query(); ?>
            </div>
        </div>

    </section><!--END BLOG SECTION-->

    <!-- PROGRAMS AND TRAININGS SECTION START-->
            <?php
            $ptcat = get_category_by_slug('pt');
            $ptcat_id = $ptcat->term_id;
            $args = array(
                'post_type' => 'product',
                //'cat' => $ptcat_id,
                'posts_per_page' => 4,
                'order' => 'ASC'
            );
           query_posts( $args );

            ?>
    <section class="pt-section col-lg-12">
        <h1 class="pt-section__title"><?php _e('Programs and Trainings', 'ladya')?></h1>
        <div class="pt-container col-lg-12">
      <?php if (have_posts()) { ?>
        <?php while ( have_posts() ) : the_post(); ?>

        <article <?php post_class('col-lg-3 col-md-4 col-sm-6 col-xs-12'); ?>>

        <?php if (has_post_thumbnail()) { ?>
                      <a href="<?php get_permalink(); ?>" class="post-thumbnail"><?php the_post_thumbnail(); ?>

                      <!--<span class="excerpt"><?php the_excerpt(); ?> </span>--></a>
                      <?php } else { ?>
                       <a href="<?php get_permalink(); ?>" class="post-thumbnail default"><img src="<?php bloginfo('template_url'); ?>/images/default-thumbnail.jpg" alt="<?php the_title(); ?>"></a>
                       <?php }; ?>

        <div class="title">

           <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
          <span class="title__date">
            <?php
              productDate();
             ?>
          </span>
          <div class="title__description">
            <?php echo the_excerpt(); ?>
          </div>
          <a class="title__button" href="<?php the_permalink(); ?>"><?php _e('Sign In', 'ladya'); ?></a>
          <div class="title__price"><?php _e('Cost: ', 'ladya') ?><span><?php
            productCost(); ?></span>

          </div>


        </div>
        </article><!-- End of pt div-->

        <?php endwhile;  ?>
        <?php wp_reset_query(); ?>
           </div>
    </section>
           <?php }; ?>
<!--PROGRAMS AND TRAININGS SECTION ENDS-->

<!--EVENTS SECTION START-->
            <?php
            $eventcat = get_category_by_slug('events');
            $eventcat_id = $eventcat->term_id;
            $args = array(
                'post_type' => 'event',
                //'cat' => $eventcat_id,
                'posts_per_page' => 3,
                'order' => 'ASC'
            );
            query_posts( $args );

?>
    <?php if (have_posts()) { ?>
     <section class="event-section col-lg-12">
        <h1 class="event-section__title"><?php _e('Upcoming Events', 'ladya')?></h1>
           <div class="event-container col-lg-12">
        <?php while ( have_posts() ) : the_post(); ?>

        <article <?php post_class('col-lg-2 col-sm-3 col-xs-6'); ?>>

        <?php if (has_post_thumbnail()) { ?>
            <a href="<?php get_permalink(); ?>" class="post-thumbnail"><?php the_post_thumbnail(); ?></a>


                      <?php } else { ?>
                       <a href="<?php get_permalink(); ?>" class="post-thumbnail default"><img src="<?php bloginfo('template_url'); ?>/images/default-thumbnail.jpg" alt="<?php the_title(); ?>"></a>
                       <?php }; ?>

        <div class="title">

           <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
            <span><?php echo get_post_meta( get_the_ID(), 'eventdata', true); ?></span>

        </div>
        </article><!-- End of pt div-->

        <?php endwhile;  ?>
        <?php wp_reset_query(); ?>

        </div>
    </section>
    <?php }; ?>
<!--EVENT SECTION ENDS-->

</div><!-- END MAIN DIV-->


<?php get_footer(); ?>
