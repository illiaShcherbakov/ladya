<?php get_header() ?>


<div class="main col-lg-10 col-xs-12">
    
    <div class="content-part col-lg-8 col-md-8">
          
           <div class="content col-lg-12">

            <?php if (have_posts()) : ?>

            <?php while (have_posts()) : the_post(); ?>


            <?php the_content(); ?>

            <?php endwhile; endif; ?>
        
            
        </div>
    </div> 
    
    <?php get_sidebar(); ?>
  
</div>

<?php get_footer(); ?>

