<?php /*  Template Name: Programms and Trainings  */ ?>


<?php get_header() ?>


<div class="main col-lg-10 col-xs-12">

   <?php the_breadcrumb(); ?>

 <?php
    //$ptcat = get_category_by_slug('pt');
    $ptcat_id = $ptcat->term_id;
    $args = array( 'post_type' => 'product' );
    query_posts( $args );

?>

       <div class="pt-section pt-page col-lg-12">
         <div class="pt-container">


        <?php while ( have_posts() ) : the_post(); ?>

        <article <?php post_class('col-lg-3 col-md-4 col-sm-6 col-xs-12'); ?>>

        <?php if (has_post_thumbnail()) { ?>
                      <a href="<?php get_permalink(); ?>" class="post-thumbnail"><?php the_post_thumbnail(); ?><span class="excerpt"><?php the_excerpt(); ?> </span></a>
                      <?php } else { ?>
                       <a href="<?php get_permalink(); ?>" class="post-thumbnail default"><img src="<?php bloginfo('template_url'); ?>/images/default-thumbnail.jpg" alt="<?php the_title(); ?>"></a>
                       <?php }; ?>

                       <div class="title">

                          <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                         <span class="title__date">
                           <?php
                             productDate();
                            ?>
                         </span>
                         <div class="title__description">
                           <?php echo the_excerpt(); ?>
                         </div>
                         <a class="title__button" href="<?php the_permalink(); ?>"><?php _e('Sign In', 'ladya'); ?></a>
                         <div class="title__price"><?php _e('Cost: ', 'ladya') ?><span><?php
                           productCost(); ?></span>

                         </div>


                       </div>
        </article><!-- End of pt div-->

        <?php endwhile;  ?>
        <?php wp_reset_query(); ?>

        </div><!-- End of pt-container-->
    </div><!-- End of pt-section-->
</div><!-- End of .main-->

<?php get_footer(); ?>
