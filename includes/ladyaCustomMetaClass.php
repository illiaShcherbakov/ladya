<?php



/**
 * Adding custom meta boxes
 */
class ladyaCusomMetaBox {

	/**
	 * Hook into the appropriate actions when the class is constructed.
	 */
	//public function __construct( $post_type, $unicname, $headline, $fieldDisc ) {
    public function __construct( $metabox ) {

  global $post;

    $this->post_type = $metabox['post_type'];
    $this->unicname = $metabox['unicname'];
    $this->headline = $metabox['headline'];
    $this->fieldDisc = $metabox['fieldDisc'];

    add_action( 'add_meta_boxes', array( $this, 'add_meta_box' ) );
    add_action( 'save_post', array( $this, 'save' ), 1, 2);


	}

	/**
	 * Adds the meta box container.
	 */
	public function add_meta_box() {

			add_meta_box(
				$this->unicname
				,$this->headline
				,array( $this, 'render_meta_box_content' )
				,$this->post_type
				,'advanced'
				,'high'
			);

	}

	/**
	 * Save the meta when the post is saved.
	 *
	 * @param int $post_id The ID of the post being saved.
	 */
	public function save($post_id, $post) {
    // verify this came from the our screen and with proper authorization,
    // because save_post can be triggered at other times
  	$retrieved_nonce = $_REQUEST[$this->unicname .  '_noncename'];
  	if ( !wp_verify_nonce( $retrieved_nonce, 'custom_box_nonce' )) {
  		return $post->ID;
  	}

  	//If the user allowed to edit the post or page
  	if ( !current_user_can( 'edit_post', $post->ID ) )  {
  		return $post->ID;
  	}

  	// OK, we're authenticated: we need to find and save the data
  	// We'll put it into an array to make it easier to loop though.
  	$events_meta[$this->unicname] = $_POST[$this->unicname];

  	// Add values of $events_meta as custom fields
  	foreach ( $events_meta as $key => $value ) {
  		if ( $post->post_type == 'revision' ) return;  // Don't store custom data twice
  		$value = implode( ',', (array)$value);// If $value is an array, make it a CSV (unlikely)
  		if ( get_post_meta( $post->ID, $key, FALSE ) ) {
  			update_post_meta( $post->ID, $key, $value );

  		} else {
  			// If the custom field doesn't have a value
  			add_post_meta( $post->ID, $key, $value );
  		}
  		if ( !$value ) delete_post_meta($post->ID, $key);// Delete if blank
  	}
	}


	/**
	 * Render Meta Box content.
	 *
	 * @param WP_Post $post The post object.
	 */
	public function render_meta_box_content( $post ) {

		// // Add an nonce field so we can check for it later.
		// wp_nonce_field( "'" . $this->unicname . "_noncename'", $this->unicname );
    //
		// // Use get_post_meta to retrieve an existing value from the database.
		// $value = get_post_meta( $post->ID, $this->unicname, true );
    //
		// // Display the form, using the current value.
		// echo '<label for="ladya_new_field">';
		// _e( $this->fieldDesc, 'ladya' );
		// echo '</label> ';
		// echo '<input type="text" id="' . $this->unicname . '" name="' . $this->unicname . '"';
		// echo ' value="' . esc_attr( $value ) . '" size="25" />';




    // Noncename needed to verify where the data originated
    //wp_create_nonce('event_action');
    echo '<input type="hidden" name="' .  $this->unicname .  '_noncename" id="' . $this->unicname . '_noncename" value="' .
    wp_create_nonce( 'custom_box_nonce' ) . '" />';


    $date = get_post_meta( $post->ID, $this->unicname, true );
    echo '<label for="' . $this->unicname . '">';
    echo __( $this->fieldDisc, 'ladya' );
    echo '</label>';
    //Echo the field
    echo '<input id="' . $this->unicname . '" type="text" name="' . $this->unicname . '" value="' . $date . '" class="widefat" />';
	}


}






?>
