<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
		<meta name="viewport" content="width=device-width, initial-scale=1"/>
        <title><?php wp_title('&laquo;', true, 'right');?> <?php bloginfo('name');?></title>

        <!--WP gerenated header-->
        <?php wp_head(); ?>
        <!--END WP gerenated header-->

        <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" />

    </head>

    <body <?php body_class(); ?> >
       <div class="wrapper">
          <div class="main-wrapper">
           <header class="header col-lg-12 col-xs-12">
               <div class="header-title">
                   <div class="header-title__logo">
                       <div class="header-title__logo__container">
                           <a href="<?php echo  get_site_url() ?>"><img src="<?php bloginfo('template_url') ?>/images/logo.png" alt=""></a>
                        </div>
                    </div>
                   <div class="header-title__description">
                       <a href="<?php echo  get_site_url(); ?>" class="header-title__description__name">Olga Ladya</a>
                       <span>Коучинг и психологические тренинги</span>
                   </div>
               </div>


                   <?php wp_nav_menu(array('theme_location' => 'main', 'fallback_cb' => '', 'container'=>'nav', 'container_class'=>'nav-main col-lg-12', 'container_id'=>'nav-main')); ?>


           </header>
