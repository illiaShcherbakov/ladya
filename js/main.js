$(document).ready(function() { 

//Sticky main nav-bar
$(window).scroll(function() {
    if ( $(this).scrollTop() > $('header').height() ) {
        $('.nav-main').addClass('nav-main-scrolled');
    } else {
        $('.nav-main').removeClass('nav-main-scrolled');
    }
});

//Slide Down and Up Sub menu
/*
$('li.menu-item-has-children').hover(function(){
   // 
   $(this).find('ul.sub-menu').slideDown(300);   
});
$('li.menu-item-has-children').mouseleave(function(){
    
    $('li.menu-item-has-children>ul.sub-menu').slideUp(300);
});
//Drop down alternative
*/
/*
$(function(){

    $(".header li.menu-item-has-children").click(function(){
        alert('Cliked!');
        $(this).addClass("hover");
        $('.header ul:first',this).css('visibility', 'visible');
    
    }, function(){
    
        $(this).removeClass("hover");
        $('.header ul:first',this).css('visibility', 'hidden');
    
    });
    
    //$("ul.dropdown li ul li:has(ul)").find("a:first").append(" &raquo; ");

}); */
    $('.header .menu>li.menu-item-has-children a').append('<span class="fa fa-caret-down"></span>');


    $(".header li.menu-item-has-children>a").click(function(){
        //alert('Cliked!');
        $('.header li.menu-item-has-children').addClass("hover");
        $('.header li.menu-item-has-children>ul').toggle('fast');
    
    });
    
    //$("ul.dropdown li ul li:has(ul)").find("a:first").append(" &raquo; ");



var pt_excerpt = $('.category-pt .post-thumbnail .excerpt');
var pt_thumbnail = $('.category-pt .post-thumbnail');

 $('.category-pt .post-thumbnail').hover(function() {
   $(this).find('.excerpt').slideDown('fast');
}, function() {
     $(this).find('.excerpt').slideUp('fast');
 });
    
$('.blog-container article').hover(function() {
   $(this).find('.description .excerpt').slideDown('');
}, function() {
     $(this).find('.description .excerpt').slideUp('');
 });


 });//End of document.ready