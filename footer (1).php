     </div> <!-- End of Page-Wrapper div -->
 </div><!--END OF WRAPPER DIV-->
 
<div class="footer-wrapper col-lg-12 col-xs-12"> 
    <div class="footer col-lg-10 col-xs-12">

 <div class="footer_container col-lg-3 col-md-4 col-sm-6 col-xs-12">
     <?php wp_nav_menu(array('theme_location' => 'footer_1', 'fallback_cb' => '', 'container'=>'nav', 'container_class'=>'footer-menu', 'container_id'=>'footer-menu_1')); ?>
</div>
     
 <div class="footer_container col-lg-3 col-md-4 col-sm-6 col-xs-12">
     <?php wp_nav_menu(array('theme_location' => 'footer_2', 'fallback_cb' => '', 'container'=>'nav', 'container_class'=>'footer-menu', 'container_id'=>'footer-menu_2')); ?>
 </div>
  
  <div class="footer_container col-lg-3 col-md-4 col-sm-6 col-xs-12">
  
   <div class="contacts">
       <div class="contacts__title"><?php _e('Contacts', 'ladya'); ?></div>
       
       <div class="contacts__wrapper">
           <div class="contacts__phone">
               <div class="contacts__phone__title"><?php _e('Phone', 'ladya'); ?></div>
               <p>(098)-311-23-41</p>
               <p>(098)-311-23-41</p>
               </div> 

               <div class="contacts__email">
                   <div class="contacts__email__title"><?php _e('Email', 'ladya'); ?></div>
                   <p>ladya@gmail.com</p>
               </div>
        </div>
               
   </div>   
   
    </div>
    <div class="footer_container col-lg-3 col-md-4 col-sm-6 col-xs-12">
   <div class="footer-sidebar col-lg-11">
       <?php if (is_active_sidebar('footer-sidebar-1')) {
    dynamic_sidebar('footer-sidebar-1');
    } ?>
   </div>
</div>
   
     </div>  
</div> <!-- END OF FOOTER WRAPPER -->
       
       <!-- End of Page div -->
        
        <?php wp_footer(); ?>
        <script src="<?php bloginfo('template_url'); ?>/js/main.js"></script>
    </body>
</html>