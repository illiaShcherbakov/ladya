<?php
require 'includes/ladyaCustomMetaClass.php';

require_once dirname( __FILE__ ) . '/class-tgm-plugin-activation.php';
add_action( 'tgmpa_register', 'ladya_register_required_plugins' );

//Install required plugins with theme activation
function ladya_register_required_plugins() {
    $plugins = array(
        array(
			'name'               => 'Contact Form 7', // The plugin name.
			'slug'               => 'contact-form-7', // The plugin slug (typically the folder name).
			'source'             => get_stylesheet_directory() . '/lib/TGM/plugins/contact-form-7.4.2.2.zip', // The plugin source.
			'required'           => true, // If false, the plugin is only 'recommended' instead of required.
			'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
			'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
			'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
			'external_url'       => '', // If set, overrides default API URL and points to an external URL.
			'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
		),

        array(
			'name'               => 'Flamingo', // The plugin name.
			'slug'               => 'flamingo', // The plugin slug (typically the folder name).
			'source'             => get_stylesheet_directory() . '/lib/TGM/plugins/flamingo.1.3.zip', // The plugin source.
			'required'           => true, // If false, the plugin is only 'recommended' instead of required.
			'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
			'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
			'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
			'external_url'       => '', // If set, overrides default API URL and points to an external URL.
			'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
		)
    );

tgmpa( $plugins, $config );
};

add_action('after_setup_theme', 'ladya_theme_setup');
function ladya_theme_setup(){
    load_theme_textdomain('ladya', get_template_directory() . '/languages');
}

add_action( 'init', 'ladya_custom_post_types' );
/**
 * Register a book post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function ladya_custom_post_types() {
    ladya_product_init();
    ladya_event_init();


};
function ladya_product_init() {
	$labels = array(
		'name'               => __( 'Products', 'product', 'ladya' ),
		'singular_name'      => __( 'Product', 'post type singular name', 'ladya' ),
		'menu_name'          => __( 'Products', 'admin menu', 'ladya' ),
		'name_admin_bar'     => __( 'Product', 'add new on admin bar', 'ladya' ),
		'add_new'            => __( 'Add New', 'product', 'ladya' ),
		'add_new_item'       => __( 'Add New Product', 'ladya' ),
		'new_item'           => __( 'New Product', 'ladya' ),
		'edit_item'          => __( 'Edit Product', 'ladya' ),
		'view_item'          => __( 'View Product', 'ladya' ),
		'all_items'          => __( 'All Product', 'ladya' ),
		'search_items'       => __( 'Search Products', 'ladya' ),
		'parent_item_colon'  => __( 'Parent Products:', 'ladya' ),
		'not_found'          => __( 'No products found.', 'ladya' ),
		'not_found_in_trash' => __( 'No products found in Trash.', 'ladya' )
	);

	$args = array(
		'labels'             => $labels,
        'description'        => __( 'Description.', 'ladya' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'product' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
	);

	register_post_type( 'product', $args );
}

function ladya_event_init() {
	$labels = array(
		'name'               => __( 'Events', 'post type general name', 'ladya' ),
		'singular_name'      => __( 'Event', 'post type singular name', 'ladya' ),
		'menu_name'          => __( 'Events', 'admin menu', 'ladya' ),
		'name_admin_bar'     => __( 'Event', 'add new on admin bar', 'ladya' ),
		'add_new'            => __( 'Add New', 'event', 'ladya' ),
		'add_new_item'       => __( 'Add New Event', 'ladya' ),
		'new_item'           => __( 'New Event', 'ladya' ),
		'edit_item'          => __( 'Edit Event', 'ladya' ),
		'view_item'          => __( 'View Event', 'ladya' ),
		'all_items'          => __( 'All Events', 'ladya' ),
		'search_items'       => __( 'Search Events', 'ladya' ),
		'parent_item_colon'  => __( 'Parent Events:', 'ladya' ),
		'not_found'          => __( 'No events found.', 'ladya' ),
		'not_found_in_trash' => __( 'No events found in Trash.', 'ladya' )
	);

	$args = array(
		'labels'             => $labels,
        'description'        => __( 'Description.', 'ladya' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'event' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
    //'register_meta_box_cb' => 'add_events_metaboxes'

	);

	register_post_type( 'event', $args );
}




/*
/---Custom Metaboxes initialization and their functions
*/
function call_createCustomMetaBox() {
	new ladyaCusomMetaBox( array(
      'post_type'=>'event',
      'unicname'=>'eventdata',
      'headline'=>'Event Date',
      'field_desc'=>'Enter the date of your event'
    ) );
}
if ( is_admin() ) {
	add_action( 'load-post.php', 'call_createCustomMetaBox' );
	add_action( 'load-post-new.php', 'call_createCustomMetaBox' );
}

function call_createEventPlaceMetabox() {
  new ladyaCusomMetaBox( array(
      'post_type'=>'event',
      'unicname'=>'eventplace',
      'headline'=>'Event Place',
      'field_desc'=>'Enter the place of your event'
    ) );
}
if ( is_admin() ) {
	add_action( 'load-post.php', 'call_createEventPlaceMetabox' );
	add_action( 'load-post-new.php', 'call_createEventPlaceMetabox' );
}

function call_createProductCostMetabox() {
  new ladyaCusomMetaBox( array(
      'post_type'=>'product',
      'unicname'=>'product_cost',
      'headline'=>'Product Cost',
      'field_desc'=>'Enter the cost of your product'
    ) );
}
if ( is_admin() ) {
	add_action( 'load-post.php', 'call_createProductCostMetabox' );
	add_action( 'load-post-new.php', 'call_createProductCostMetabox' );
}

function call_createProductDatesMetabox() {
  new ladyaCusomMetaBox( array(
      'post_type'=>'product',
      'unicname'=>'product_date',
      'headline'=>'Date Range',
      'field_desc'=>'Enter the date range for your product'
    ) );
}
if ( is_admin() ) {
	add_action( 'load-post.php', 'call_createProductDatesMetabox' );
	add_action( 'load-post-new.php', 'call_createProductDatesMetabox' );
}

function productCost() {
  echo get_post_meta( get_the_ID(), 'product_cost', true);
}
function productDate() {
  echo get_post_meta( get_the_ID(), 'product_date', true);
}
/*
/--- End of Custom Metaboxes
*/

add_theme_support( 'post-thumbnails' );

//Exclude wp's qjuery and include our own
if( !is_admin()){
	wp_deregister_script('jquery');
	wp_register_script('jquery', ("http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"), false, '1.3.2');
	wp_enqueue_script('jquery');
}

//Add scripts and stylesheet

function ladya_styles() {
    wp_enqueue_style('bootstrap', get_template_directory_uri() . '/vendor/bootstrap/css/bootstrap.min.css');
    wp_enqueue_script('bootstrapjs', get_template_directory_uri() . '/vendor/bootstrap/js/bootstrap.min.js');
    wp_enqueue_style('reset', get_template_directory_uri() . '/css/reset.css');
    wp_enqueue_style('font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css');

}
add_action('wp_enqueue_scripts', 'ladya_styles');

//Delete 3 dots in the end of the excerpt
function new_excerpt_more( $more ) {
	return '';
}
add_filter('excerpt_more', 'new_excerpt_more');



add_action( 'after_setup_theme', 'register_main_menu' );
function register_main_menu() {
  register_nav_menu( 'main', __( 'Primary Menu', 'ladyatheme' ) );
}

add_action( 'after_setup_theme', 'register_footer_menu_1' );
function register_footer_menu_1() {
  register_nav_menu( 'footer_1', __( 'Foote Menu 1', 'ladyatheme' ) );
}

add_action( 'after_setup_theme', 'register_footer_menu_2' );
function register_footer_menu_2() {
  register_nav_menu( 'footer_2', __( 'Foote Menu 2', 'ladyatheme' ) );
}

add_action( 'after_setup_theme', 'register_blog_menu' );
function register_blog_menu() {
  register_nav_menu( 'blog-menu', __( 'Blog Menu', 'ladyatheme' ) );
}



    register_sidebar( array(
        'name' => 'Footer Sidebar 1',
        'id' => 'footer-sidebar-1',
        'description' => 'Appears in the footer area',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="widget-title">',
        'after_title' => '</h4>',
    ));

        register_sidebar( array(
        'name' => 'Main Sidebar',
        'id' => 'main-sidebar',
        'description' => 'Appears in the blog main area',
        'before_widget' => '<div id="%1$s" class="widget col-lg-12 col-md-6 col-sm-4 %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="widget-title">',
        'after_title' => '</h4>',
    ));

//add_action('widget_init', 'ladyatheme_widget_init');

function the_breadcrumb() {
    echo '<div class="breadcrumbs">';
    echo '<a href="';
    echo get_option('home');
    echo '">';
     _e('Home', 'ladya');
    echo '</a>&#10093;';
    if ( is_category() || is_single() ) {

         the_category(' ');
        if ( is_single() ) {
            echo '&#10093;';
             echo '<p>';
            echo the_title();
            echo '</p>';
        }
    }
    elseif ( is_page() ) {
         echo '<p>';
        echo the_title();
        echo '</p>';
    }
     else {
    echo 'Home';
     }
  echo '</div>';
}

//Related posts by tag
// Related Posts Function, matches posts by tags - call using ladya_related_posts(); )
function ladya_tag_related_posts() {
    global $post;
    $tags = wp_get_post_tags( $post->ID );
    if($tags) {
        foreach( $tags as $tag ) {
            $tag_arr .= $tag->slug . ',';
        }
        $args = array(
            'tag' => $tag_arr,
            'posts_per_page' => 5, /* You can change this to show more */
            'post__not_in' => array($post->ID)
        );
        $related_posts = get_posts( $args );
        if($related_posts) {
            echo '<section class="related col-lg-12">';
            echo '<h4 class="related__title">Related Posts</h4>';
            echo '<div class="related__posts col-lg-12">';
            foreach ( $related_posts as $post ) : setup_postdata( $post ); ?>
                <article <?php post_class('col-lg-4 col-md-3 col-sm-3 col-xs-6'); ?>>

            <?php if (has_post_thumbnail()) { ?>
            <a href="<?php get_permalink(); ?>" class="post-thumbnail"><?php the_post_thumbnail(); ?></a>
                      <?php } else { ?>
                       <a href="<?php get_permalink(); ?>" class="post-thumbnail default"><img src="<?php bloginfo('template_url'); ?>/images/default-thumbnail.jpg" alt="<?php the_title(); ?>"></a>
                       <?php }; ?>

        <div class="title">
           <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
           <div class="post-info">

                        <span class="post-info__date"><?php the_time('jS F Y'); ?></span>
                </div>
        </div>

        </article>

        <?php endforeach; } else {
            return null;
        }
            } else {
        return null;
    };
    wp_reset_postdata();
    echo '</div>';
    echo '</section>';
}

//Related post by by category
// Related Posts Function, matches posts by tags - call using joints_related_posts(); )
function ladya_category_related_posts() {
    global $post;
    $categories  = get_the_category( $post->ID );
    if($categories) {
        $category_ids = array();
        foreach( $categories as $individual_categories ) {
            $category_ids[] = $individual_categories->term_id;
        }
        $args = array(
            'category__in' => $category_ids,
            'posts_per_page' => 5, /* You can change this to show more */
            'post__not_in' => array($post->ID)
        );
        $related_posts = get_posts( $args );
        if($related_posts) {
        echo '<h4>Related Posts</h4>';
        echo '<ul id="ladya-sb-related-posts">';
            foreach ( $related_posts as $post ) : setup_postdata( $post ); ?>
                <li class="related_post">
                    <a class="entry-unrelated" href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
                    <?php get_template_part( 'partials/content', 'byline' ); ?>
                </li>
            <?php endforeach; }
            }
    wp_reset_postdata();
    echo '</ul>';
}


//Last new posts for sidebar
// Related Posts Function, matches posts by tags - call using joints_related_posts(); )
function ladya_new_related_posts() {

    if (!is_page_template('ladya-blog.php') ) {

    global $post;

        $args = array(
            //'category__in' => $category_ids,
            'posts_per_page' => 5, /* You can change this to show more */
            'post__not_in' => array($post->ID)
        );
        $related_posts = get_posts( $args );
        if($related_posts) {
            echo '<div class="widget widget-new_posts col-lg-12">';
        echo '<h4 class="widget-title">' . __('New Posts','ladya') . '</h4>';
        echo '<ul id="ladya-sb-related-posts">';
            foreach ( $related_posts as $post ) : setup_postdata( $post ); ?>
                    <?php if (has_post_thumbnail()) { ?>
            <a href="<?php get_permalink(); ?>" class="post-thumbnail"><?php the_post_thumbnail(); ?></a>
                      <?php } else { ?>
                       <a href="<?php get_permalink(); ?>" class="post-thumbnail default"><img src="<?php bloginfo('template_url'); ?>/images/default-thumbnail.jpg" alt="<?php the_title(); ?>"></a>
                       <?php }; ?>
                   <li class="related_post"><span class="fa fa-chevron-right"></span>
                    <a class="entry-unrelated" href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>

                </li>
            <?php endforeach; }

    wp_reset_postdata();
        };
    echo '</ul>';
    echo '</div>';
}







?>
