
<aside class="main-sidebar col-lg-4">

       <?php if (is_active_sidebar('main-sidebar')) :  ?>
    <div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">

         <?php dynamic_sidebar('main-sidebar');?>
    </div>
    <?php  else :  ?>
    no side bar
    <?php endif; ?>

    <?php ladya_new_related_posts(); ?>

</aside>
