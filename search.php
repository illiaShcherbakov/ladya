<?php
/*
Template Name: Search Page
*/
?>

<?php  get_header(); ?>

<div class="main">
    <div class="content">

 <?php if (have_posts()) : ?>
               <?php while (have_posts()) : the_post(); ?>

    <article <?php post_class(); ?> >
                      
                <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>  
                
                 <div class="post-info">
                    
                       <span class="post-info__category"><?php _e('Posted in', 'ladya'); echo ' '; the_category(', '); ?> </span>
                        <span class="post-info__date"><?php the_time('jS F Y'); ?></span>
                   
                </div>
                    
                <?php if (has_post_thumbnail()) { ?>
                      <a href="<?php get_permalink(); ?>" class="post-thumbnail"><?php the_post_thumbnail(); ?></a>
                      <?php } else { ?>
                       <a href="<?php get_permalink(); ?>" class="post-thumbnail default"><img src="<?php bloginfo('template_url'); ?>/images/default-thumbnail.jpg" alt="<?php the_title(); ?>"></a>
                       <?php }; ?>
            
                <?php the_excerpt('');  ?>
                <p class="post-info__read-more"><a href="<?php get_permalink(); ?>"><?php _e('Read more', 'ladyatheme'); ?></a></p>
                      
            </article> <!-- End of the posts -->
            
                  
            <?php endwhile; 
             wp_reset_query(); ?>
             
             
            <?php if ( have_posts() ) { ?>    
            div class="pagination">
            
<div class="older"><?php next_posts_link( __('Older Posts', 'ladya') . '<span class="fa fa-angle-double-right"></span>', $the_query->max_num_pages );?></div>
                <div class="newer"><?php previous_posts_link( '<span class="fa fa-angle-double-left"></span>' . __('Newer Posts', 'ladya')) ;?></div>

            </div>
            
            <?php } else { ?>
            
            <div class="nothing">
                <h2><?php _e('Nothing Found', 'ladyatheme') ?></h2>
                <p><?php _e('Sorry, but you are looking for something that isnt here.', 'ladyatheme') ?></p>
                <p><a href="<?php echo get_option('home'); ?>"><?php _e('Return to the homepage', 'ladyatheme') ?></a></p>
            </div>
            
            <?php } ?>
            
        <?php endif; ?>


    </div><!-- End of Content-->
    <?php get_sidebar(); ?>
</div><!-- End of Content-->

<?php get_footer(); ?>