<?php /*  Template Name: Blog  */ ?>


<?php get_header(); ?>

<div class="main col-lg-10 col-xs-12">
   
    <?php  the_breadcrumb(); ?>
    
    <div class="content-part col-lg-8 col-md-8">
       
        <div class="content col-lg-12">
    
    <?php 
    $ptcat = get_category_by_slug('pt');
    $ptcat_id = $ptcat->term_id;
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    
    $args = array( 
        'cat' => -$ptcat_id,
        'paged' => $paged
    );
    query_posts( $args );

    if ( have_posts() ) { 
    //$lastpost = get_post($args);
    while ( have_posts() ) : the_post(); ?>
        
      
       
               <article <?php post_class('col-lg-12'); ?> >
                      
                <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>  
                
               <div class="post-info">
                    
                       <span class="post-info__category"><?php _e('Posted in', 'ladya'); echo ' '; the_category(', '); ?> </span>
                        <span class="post-info__date"><?php the_time('j.m.Y'); ?></span>
                   
                </div>
                <?php if (has_post_thumbnail()) { ?>
                      <a href="<?php get_permalink(); ?>" class="post-thumbnail"><?php the_post_thumbnail(); ?></a>
                      <?php } else { ?>
                       <a href="<?php get_permalink(); ?>" class="post-thumbnail default"><img src="<?php bloginfo('template_url'); ?>/images/default-thumbnail.jpg" alt="<?php the_title(); ?>"></a>
                       <?php }; ?>
            
                <?php the_excerpt('');  ?>
                <p class="post-info__read-more"><a href="<?php get_permalink(); ?>"><?php _e('Read more', 'ladya'); ?></a></p>
                      
            </article> <!-- End of the posts -->
            
                  
            <?php endwhile;  ?>
             
             
                
            <div class="pagination col-lg-12">
            
<div class="older"><?php next_posts_link( __('Older Posts', 'ladya') . '<span class="fa fa-angle-double-right"></span>', $the_query->max_num_pages );?></div>
                <div class="newer"><?php previous_posts_link( '<span class="fa fa-angle-double-left"></span>' . __('Newer Posts', 'ladya')) ;?></div>

            </div>
            
            <?php } else { ?>
            
            <div class="nothing">
                <h2><?php __('Nothing Found', 'ladya') ?></h2>
                <p><?php __('Sorry, but you are looking for something that isnt here.', 'ladya') ?></p>
                <p><a href="<?php echo get_option('home'); ?>"><?php __('Return to the homepage', 'ladya') ?></a></p>
            </div>
            
            <?php }; wp_reset_query(); ?>          
            
            
        </div> <!-- End of Content-->
    
    </div> <!-- End of Content-part-->
    
    <?php get_sidebar(); ?>
    
</div><!--End of Main div -->

<?php get_footer(); ?>

