<?php get_header(); ?>


<div class="main col-lg-10 col-md-10 col-sm-12 col-xs-12">

  <?php the_breadcrumb(); ?>
   <div class="content-part col-lg-8 col-md-8 col-sm-12 col-xs-12">

    <div class="content col-lg-12">

        <?php if(have_posts()) :
              while (have_posts()) : the_post();

        ?>

        <article <?php post_class('col-lg-12'); ?> >
                      <h1><?php the_title(); ?></h1>

                <div class="post-info">

                       <span class="post-info__category"><?php _e('Posted in', 'ladya'); echo ' '; the_category(', '); ?> </span>
                        <span class="post-info__date"><?php the_time('j.m.Y'); ?></span>

                </div>

                <?php if (has_post_thumbnail()) { ?>
                      <a href="<?php get_permalink(); ?>" class="post-thumbnail"><?php the_post_thumbnail(); ?></a>
                      <?php } else { ?>
                       <a href="<?php get_permalink(); ?>" class="post-thumbnail default"><img src="<?php bloginfo('template_url'); ?>/images/default-thumbnail.jpg" alt="<?php the_title(); ?>"></a>
                       <?php }; ?>

                       <?php the_content(); ?>

                       <?php endwhile; ?>

                       <?php else : ?>

            <div class="nothing">
                <h2><?php __('Nothing Found', 'ladya') ?></h2>
                <p><?php __('Sorry, but you are looking for something that isnt here.', 'ladya') ?></p>
                <p><a href="<?php echo get_option('home'); ?>"><?php __('Return to the homepage', 'ladya') ?></a></p>
            </div>

            <?php endif; ?>

            <!--Related posts by tag-->

                      <!--End oа Related posts-->

        </article><!-- End of post div -->



    </div><!-- End of content div -->

   <?php ladya_tag_related_posts(); ?>

   </div> <!--End of Content-part-->

    <?php get_sidebar(); ?>


</div><!-- End of main div -->


<?php get_footer(); ?>
